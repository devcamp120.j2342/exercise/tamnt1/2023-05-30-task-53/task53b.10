import model.Author;
import model.Book;

public class App {
    public static void main(String[] args) throws Exception {
        Author author1 = new Author("Tam", "tamkaka113@gmail.com", 'm');
        Author author2 = new Author("Adam", "tamnguyen124@gmail.com", 'f');

        System.out.println(author1.toString());
        System.out.println(author2.toString());

        Book book1 = new Book("book1", author1, 10000);
        Book book2 = new Book("book2", author2, 5000, 10);

        System.out.println(book1.toString());
        System.out.println(book2.toString());
    }
}
